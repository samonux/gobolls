//package net.gametutorial.shoottheduck;
package com.example.goball;

import android.graphics.Canvas;
import android.graphics.Rect;

public class Duck {

	// This are starting data.
	public static final float initSpeed = 5;
	public static final long initTimeBetweenDucks = 1800; // in milliseconds

	// This is current speed that will be increased and current time that will
	// be decreased.
	public static float speed;
	public static long timeBetweenDucks; // in milliseconds

	public static long timeOfLastDuck;

	// Ducks will be coming from left and right, we will use this to know which
	// direction is next.
	public static boolean direction = true;

	// Needed for speeding up the game
	public static long timeBetweenSpeedups = 250; // in milliseconds
	public static long timeOfLastSpeedup;

	// Duck position on the screen.
	public float x;
	public float y;

	// Speed and direction.
	private float velocityY;
	private float velocity;
	private boolean suicide;
	public Duck(int x, int y ){
		
		
	}
	public Duck(int y) {
		suicide = false;
		this.y = y;

		if (Duck.direction) {
			this.x = Game.screenWidth - Game.duckImage.getWidth();
			velocity = speed * -1;
		} else {
			this.x = 0;// - Game.duckImage.getWidth();
			velocity = speed;
		}
		this.velocityY = 20;

		// We change direction for a next duck.
		Duck.direction = !Duck.direction;
	}

	public void checkBorders() { // + Game.duckImage.getWidth()
		if (this.x > Game.screenWidth - Game.duckImage.getWidth()
				|| (this.x < 0)) {
			this.velocity *= -1;
			this.velocity -= 10;
		}
		if (this.y > Game.screenHeight - Game.duckImage.getHeight()
				|| this.y < 0) {
			this.velocityY *= -1;
			this.velocityY -= 10;
		}
	}

	// public boolean checkScore() { // + Game.duckImage.getWidth()
	// if (this.x > Game.screenWidth - 2 * Game.duckImage.getWidth()
	// && this.x < Game.duckImage.getWidth()
	// && this.y > Game.screenHeight
	// && this.y < Game.duckImage.getHeight())
	// return true;
	// return false;
	// }
	/**
	 * Checks if the duck was touched/shoot.
	 * 
	 * @param touchX
	 *            X coordinate of the touch.
	 * @param touchY
	 *            Y coordinate of the touch.
	 * 
	 * @return True if touch coordinates are in the coordinates of duck
	 *         rectangle, false otherwise.
	 */
	public boolean wasItShoot(int touchX, int touchY) {
		// Rect duckRect = new Rect((int) this.x, (int) this.y, (int) this.x
		// + Game.duckImage.getWidth(), (int) this.y
		// + Game.duckImage.getHeight());
		Rect duckRect = new Rect(Game.screenWidth / 4
				+ Game.duckImage.getWidth(), 0, Game.screenWidth - 2
				* Game.duckImage.getWidth(), Game.grassImage.getHeight());
		return duckRect.contains(touchX, touchY);
	}

	public void wasClickedB(int touchX, int touchY){
		this.velocity += touchX;
		this.velocityY += touchY;
	}
	public void wasClicked(int touchX, int touchY) {
		// p1
		// Rect r1= new Rect((int) this.x,
		// (int) this.y,
		// (int) this.x + Game.duckImage.getWidth(),
		// (int) this.y + Game.duckImage.getHeight());
		Rect r1 = new Rect((int) this.x, (int) this.y, (int) this.x
				+ Game.duckImage.getWidth() / 2, (int) this.y
				+ Game.duckImage.getHeight() / 2);

		Rect r2 = new Rect((int) this.x + Game.duckImage.getWidth() / 2,
				(int) this.y, (int) this.x + Game.duckImage.getWidth(),
				(int) this.y + Game.duckImage.getHeight() / 2);

		Rect r3 = new Rect((int) this.x, (int) this.y
				+ +Game.duckImage.getHeight() / 2, (int) this.x
				+ Game.duckImage.getWidth() / 2, (int) this.y
				+ Game.duckImage.getHeight());

		Rect r4 = new Rect((int) this.x + Game.duckImage.getWidth() / 2,
				(int) this.y + Game.duckImage.getHeight() / 2, (int) this.x
						+ Game.duckImage.getWidth(), (int) this.y
						+ Game.duckImage.getHeight());
		// top left
		int speedX = 15;
		int speedY = 15;

		if (r1.contains(touchX, touchY)) {
			speedX *= 1;
		}
		// top right
		if (r2.contains(touchX, touchY)) {
			speedX *= -1;
		}

		// bottom left
		if (r3.contains(touchX, touchY)) {
			speedY *= 1;
		}
		
		if (r4.contains(touchX, touchY)) {
			speedY *= 1;
		}
		
		// buttom right
		if (r3.contains(touchX, touchY)) {
			speedX *= -1;
			speedY *= -1;

		}
		this.velocity += speedX;
		this.velocityY += speedY;

	}

	/**
	 * Move the duck.
	 */
	public void update() {
		this.y += velocityY;
		this.x += velocity;
		checkBorders();
		if (wasItShoot((int) this.x, (int) this.y)) {
			// remove set pic nul
			suicide = true;
		}
	}

	public boolean isDead() {
		return suicide;
	}

	/**
	 * Draw the duck to a screen.
	 * 
	 * @param canvas
	 *            Canvas to draw on.
	 */
	public void draw(Canvas canvas) {
		if (velocity < 0)
			canvas.drawBitmap(Game.duckImage, x, y, null);
		else
			canvas.drawBitmap(Game.duckRightImage, x, y, null);
	}

}
