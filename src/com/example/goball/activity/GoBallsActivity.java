package com.example.goball.activity;



import android.app.Activity;
import android.os.Bundle;

import com.example.goball.GoBallsAplication;
import com.example.goball.application.Game;
import com.example.goball.application.Player;

public class GoBallsActivity extends Activity {
	protected final static String TAG = "balls";
	
	protected Game game;
	protected Player systemPlayer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		GoBallsAplication goBallsAplication = (GoBallsAplication) getApplication();
		game = goBallsAplication.getGame();
		systemPlayer = goBallsAplication.getPlayer();
	}
	
	public Game getGame(){
		return game;
	}
	
	public Player getPlayer(){
		return systemPlayer;
	}
	
	public void setGame(Game game){
		this.game = game;
	}
}
