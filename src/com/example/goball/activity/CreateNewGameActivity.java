package com.example.goball.activity;

import java.util.Calendar;
import java.util.Random;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.goball.R;
import com.example.goball.application.Game;
import com.example.goball.application.Player;
public class CreateNewGameActivity extends GoBallsActivity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_new_game);
		
		((EditText) findViewById(R.id.playerName)).setHint(getPlayer().getName());
		if (savedInstanceState == null) {
		}
	}

	public void back(View v) {
		// Perform action on click
		startActivity(new Intent(this, MainActivity.class));
	}

	public void play(View v) {
		// create game
		String gameName = ((EditText) findViewById(R.id.gameName)).getText().toString();
		int numOfPlayers =5;
		//int numOfPlayers = (EditText) findViewById(R.id.textLastName);
		Game game = getGame();
		game.clear();
		if(gameName.equals(""))
			gameName = "Default player";
		game.setName(gameName);
		game.setNumOfPlayers(numOfPlayers);
		
		//create player
		Player p = getPlayer();
		String playerName = ((EditText) findViewById(R.id.playerName)).getText().toString();
		if(playerName.isEmpty()&&(p.getName().isEmpty()))
				playerName = "Default player";
		if(playerName.isEmpty()&&!(p.getName().isEmpty()))
			playerName = p.getName();
		p.setColor(getRandomColor());
		p.setName(playerName);
		
		getGame().addPlayer(p);
		startActivity(new Intent(this, LobbyActivity.class));
	}

	private static int getRandomColor() {
		Random rand = new Random();
		int red = rand.nextInt();
		int green = rand.nextInt();
		int blue = rand.nextInt();
		return Color.rgb(red, green, blue);
   }
	
	
	public static class TimePickerFragment extends DialogFragment implements
			TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			// Do something with the time chosen by the user
		}
	}

}
