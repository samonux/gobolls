package com.example.goball.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.goball.R;
import com.example.goball.application.PlayerListAdapter;

public class LobbyActivity extends GoBallsActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lobby_layout);
		TextView gameName = (TextView) findViewById(R.id.lobbyGameName);
		gameName.setText(getGame().getName());
		
		PlayerListAdapter adapter = new PlayerListAdapter(this, getGame());
		((ListView) this.findViewById(R.id.lobby_player_list)).setAdapter(adapter);
		if (savedInstanceState == null) {
		}
	}

	public void back(View v) {
        // Perform action on click
    	startActivity(new Intent(this, MainActivity.class));
    }
	public void play(View v) {
        // Perform action on click
    	//startActivity(new Intent(this, LobbyActivity.class));
		//start the sctual game
		startActivity(new Intent(this, MainMenu.class));
    }
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		getGame().removePlayer(getPlayer());
		super.onBackPressed();
	}
	

}


