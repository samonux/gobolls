package com.example.goball.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.example.goball.R;
import com.example.goball.android.wifidirect.discovery.WiFiServiceDiscoveryActivity;
import com.example.goball.application.Game;
import com.example.goball.application.GameListAdapter;

public class MainActivity extends GoBallsActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		GameListAdapter adapter = new GameListAdapter(this);
		adapter.add(new Game("game1", 2));
		adapter.add(new Game("game3", 3));
		 ((ListView) this.findViewById(R.id.games_list)).setAdapter(adapter);

	}

	
	

	public void createNewGame(View v) {
		// Perform action on click
		startActivity(new Intent(this, CreateNewGameActivity.class));
	}

	public void wifiDiscover(View v) {
		// Perform action on click
		startActivity(new Intent(this, WiFiServiceDiscoveryActivity.class));
	}


	
	
}
