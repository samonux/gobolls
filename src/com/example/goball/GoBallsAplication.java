/**
 * 
 */
package com.example.goball;

import com.example.goball.application.Game;
import com.example.goball.application.Player;

import android.app.Application;

/**
 * @author RENT1
 *
 */
public class GoBallsAplication extends Application {
	
	private Game game;
	private final Player systemPlayer = new Player();
	
	@Override
	public void onCreate() {
		super.onCreate();
		game = new Game("",0);
	}

	public Game getGame() {
		return game;
	}

	public Player getPlayer() {
		return systemPlayer;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
}
