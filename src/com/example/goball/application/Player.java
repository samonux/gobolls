/**
 * 
 */
package com.example.goball.application;

import java.util.UUID;



public class Player {

	// VARS
	String id;
	String name;
	int color;
	
	public Player(){
		this.name="default player";
		this.color=0;
		id = UUID.randomUUID().toString();
	}
	public Player (String name, int color) {
		this.name=name;
		this.color=color;
		id = UUID.randomUUID().toString();
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
	public String id(){
		return id;
	}
}
