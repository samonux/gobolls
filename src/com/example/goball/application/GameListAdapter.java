package com.example.goball.application;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.goball.R;


public class GameListAdapter extends ArrayAdapter<Game> {

	private Activity activity;

	public GameListAdapter(Activity activity) {
		super(activity, R.layout.game_list_item);
		this.activity = activity;
		refresh();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.game_list_item, parent,
					false);
		}
		final Game game = getItem(position);
		
		((TextView) convertView.findViewById(R.id.game_name))
				.setText(game.getName());
		((TextView) convertView.findViewById(R.id.game_state))
		.setText(game.players()+" \\ "+game.getNumOfPlayers());
//		((ImageButton) convertView.findViewById(R.id.buttonEditContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						new EditContactDialog(profile, new Contact(contact),
//								PlayerListAdapter.this).show(
//								activity.getFragmentManager(), "edit contact");
//					}
//				});
//		((ImageButton) convertView.findViewById(R.id.buttonRemoveContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						remove(contact);
//						profile.removeContact(contact.getChildId());
//					}
//				});
		return convertView;
	}

	public void refresh() {
		clear();
	}
}