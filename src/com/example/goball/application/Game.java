/**
 * 
 */
package com.example.goball.application;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

public class Game {

	// VARS
	int numOfPlayers;
	String name;
	Map<String, Player> players;

	public Game() {
		this.name = "";
		this.numOfPlayers = 0;
		players = new HashMap<String, Player>();
	}

	public Game(String name, int numOfPlayers) {
		this.name = name;
		this.numOfPlayers = numOfPlayers;
		players = new HashMap<String, Player>();
	}

	public int getNumOfPlayers() {
		return numOfPlayers;
	}

	public void setNumOfPlayers(int numOfPlayers) {
		this.numOfPlayers = numOfPlayers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void clear() {
		players.clear();
		;
	}

	public boolean addPlayer(Player player) {
		if (players.size() < numOfPlayers) {
			players.put(player.id(), player);
			return true;
		}
		Log.i("balls", "game full");
		return false;
	}

	public int players(){
		return players.size();
	}
	
	public void removePlayer(Player player) {
		players.remove(player.id());
	}

	public Collection<Player> getAllPlayers() {
		return players.values();
	}
}
