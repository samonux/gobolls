package com.example.goball.application;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.goball.R;


public class DeviceListAdapter extends ArrayAdapter<WifiP2pDevice> {

	private Activity activity;

	public DeviceListAdapter(Activity activity, List<WifiP2pDevice> list) {
		super(activity, R.layout.game_list_item);
		this.activity = activity;
		addAll(list);
		refresh();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.game_list_item, parent,
					false);
		}
		final WifiP2pDevice device = getItem(position);
		
		((TextView) convertView.findViewById(R.id.game_name))
				.setText(device.deviceName);
//		((ImageButton) convertView.findViewById(R.id.buttonEditContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						new EditContactDialog(profile, new Contact(contact),
//								PlayerListAdapter.this).show(
//								activity.getFragmentManager(), "edit contact");
//					}
//				});
//		((ImageButton) convertView.findViewById(R.id.buttonRemoveContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						remove(contact);
//						profile.removeContact(contact.getChildId());
//					}
//				});
		return convertView;
	}

	public void refresh() {
		clear();
	}
}