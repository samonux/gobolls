package com.example.goball.application;

import com.example.goball.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;


public class PlayerListAdapter extends ArrayAdapter<Player> {

	private Activity activity;
	private Game game;

	public PlayerListAdapter(Activity activity, Game game) {
		super(activity, R.layout.lobby_list_item);
		this.activity = activity;
		this.game = game;
		refresh();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.lobby_list_item, parent,
					false);
		}
		final Player player = getItem(position);
		
		((TextView) convertView.findViewById(R.id.game_name))
				.setText(player.getName());
		((ImageButton) convertView.findViewById(R.id.buttonPlayerColor))
		.setBackgroundColor(player.getColor());;
//		((ImageButton) convertView.findViewById(R.id.buttonEditContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						new EditContactDialog(profile, new Contact(contact),
//								PlayerListAdapter.this).show(
//								activity.getFragmentManager(), "edit contact");
//					}
//				});
//		((ImageButton) convertView.findViewById(R.id.buttonRemoveContact))
//				.setOnClickListener(new View.OnClickListener() {
//					@Override
//					public void onClick(View view) {
//						remove(contact);
//						profile.removeContact(contact.getChildId());
//					}
//				});
		return convertView;
	}

	public void refresh() {
		clear();
		addAll(game.getAllPlayers());
	}
}